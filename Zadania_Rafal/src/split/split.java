package split;

import java.util.Scanner;

public class split {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int initial = sc.nextInt();
		sc.close();
		String secondary = Integer.toString(initial);

		String[] split = secondary.split("");

		int sum = 0;
		for (String string : split) {
			sum += Integer.parseInt(string);
		}

		System.out.println(sum);
	}
}
