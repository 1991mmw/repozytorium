package GuessGame;

import java.util.Scanner;

public class GuessGame {
	
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int easy = 10, medium = 100, hard = 1000;
		System.out.println("Welcome to Guess the Number. Enter difficulty (Easy, Medium, Hard)");
		String difficulty = sc.nextLine();
		int Target = 0;
		int diff = 0;
		if(difficulty.equalsIgnoreCase("Easy")){
			Target = (int)(Math.random()*easy);
			diff = 10;
		} else if(difficulty.equalsIgnoreCase("Medium")) {
			Target = (int)(Math.random()*medium);
			diff = 100;
		} else if(difficulty.equalsIgnoreCase("Hard")) {
			Target = (int)(Math.random()*hard);
			diff = 1000;
		}	
		System.out.println("Guess the number between 0 and " + diff);
		
		boolean correct = true;
		int Guesses = 0;
		while(correct) {
			Guesses++;
			int guess = -1;
			guess = sc.nextInt();
			if (guess > diff) {
				System.out.println("Woah! That number is outside your difficulty level. Try Again");
				continue;
			}
			if(guess > Target) {
				System.out.println("Unlucky! The number to guess is less than " + guess);
			} else if(guess < Target) {
				System.out.println("Unlucky! The number to guess is more than " + guess);
			}
			
			if(guess == Target){
				System.out.println("Success! It took you " + Guesses + " guesses.");
				break;
			}
			  	
		} 
		
	} 

}
