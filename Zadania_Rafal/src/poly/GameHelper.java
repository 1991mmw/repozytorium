package poly;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GameHelper {
	
	

	public String getUserInput(String string) {
		
		String inputLine = null;
		System.out.println(string + " ");
		
		try {
			BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
			inputLine = sc.readLine();
			if(inputLine.length()==0) {
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return inputLine;
		
		
		
		
		/*Scanner helper = new Scanner(System.in);
		String guess = helper.nextLine();
		helper.close();
		return guess;*/
	}

}
