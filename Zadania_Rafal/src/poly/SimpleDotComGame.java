package poly;

public class SimpleDotComGame {

	public static void main(String[] args) {
		
		int numOfGuesses = 0;
		
		SimpleDotCom newdot = new SimpleDotCom();
		
		GameHelper helper = new GameHelper();
		
		int randomNum = (int) (Math.random()*5);
		
		
		int[] locations = {randomNum,randomNum+1,randomNum+2};
		
		newdot.setLocationCells(locations);
		
		boolean isAlive = true;
		
		while(isAlive == true) {
			
			String guess = helper.getUserInput("enter a number");
			String result = newdot.checkYourself(guess);
			numOfGuesses++;
			
			if(result.equals("kill")) {
				isAlive = false;
				System.out.println("You took " + numOfGuesses + " guesses.");
			}
			
			
			
		}
		

	}

}
