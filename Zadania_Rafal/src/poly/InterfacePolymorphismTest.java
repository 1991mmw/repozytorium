package poly;
/**
 * 1.An interface reference variable can refer to an bobject that implements the interface
 * 2.A real usage of the above scenario using methods
 * 3. Type casting in action
 */

public class InterfacePolymorphismTest {
	public static void main(String[] args) {
		
		Measurable r1 = new Rectangle(10,20);		
		Rectangle r2 = new Rectangle(10,20);
		if(r1 instanceof Circle) {
			System.out.println("circle");
		}
		if(r1 instanceof Rectangle) {
			System.out.println("Rectangle");
		}
		
		
	}
	
	public static void addArea(Measurable m1, Measurable m2){
		double area = m1.getArea()+m2.getArea();
		System.out.println(area + " Total Area");
		
		
	}


}
interface Measurable {
	float PI = 3.14F;
	public double getPerimeter();
	public double getArea();
}
class Rectangle implements Measurable {
	
	public Rectangle(int width, int length) {
		this.length = length;
		this.width = width;
	}
	
	int width, length;
	
	public void display(){
		
	}

	@Override
	public double getPerimeter() {
		return 2*(width+length);
	}

	@Override
	public double getArea() {
		return width * length;
	}
}
class Circle implements Measurable {
	int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}

	@Override
	public double getPerimeter() {
		return 2 * PI * radius;
	}

	@Override
	public double getArea() {
		return PI * radius * radius;
	}
	
}
