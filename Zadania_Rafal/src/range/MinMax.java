package range;

import java.util.Arrays;
import java.util.Scanner;

public class MinMax {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("How many numbers do you want to enter?");
		int quantity = scanner.nextInt();
		int[] array = new int[quantity];
		System.out.println("Enter " + quantity + " numbers to find their minimum and maximum value.");
		for(int i = 0 ; i < array.length ; i++){
			array[i] = scanner.nextInt();
		}
//		int[] array = {9,2,7,3,5,4,2,6,81,9,2};
		scanner.close();
		Arrays.sort(array);
		
		int min = Integer.valueOf(array[0]);
		int max = Integer.valueOf(array[array.length-1]);
		
		System.out.println("Max value is " + max);
		System.out.println("Min value is " + min);
	}
}
