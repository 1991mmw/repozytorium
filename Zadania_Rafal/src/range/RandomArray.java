package range;

import java.util.Arrays;
import java.util.Scanner;

public class RandomArray {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the size of the array:");
		int quantity = scanner.nextInt();
		int[] array = new int[quantity];
		for(int i = 0; i < array.length ; i++){
			array[i] = (int) (Math.random()*100);
		} 
		System.out.println(Arrays.toString(array));
	}
}
