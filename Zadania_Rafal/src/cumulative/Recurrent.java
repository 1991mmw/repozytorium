package cumulative;

public class Recurrent {

	public static void main(String[] args) {
		
		int n = 3;
		int sum = 0;
		for(int i = n; i >= 0 ; i--) {
			sum += (n-i);
		}
		System.out.println(sum);
	}

}
