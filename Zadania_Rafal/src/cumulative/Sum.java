package cumulative;

import java.util.Scanner;

public class Sum {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number you want to find");
		int number = scanner.nextInt();
		scanner.close();
		int sum = 0;
		for(int i = 0; i <= number ; i++) {
			sum += i;
		}
		System.out.println(sum);
		
	}

}
