package fibo;

public class Fibo {
	
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		for( int n = 0; n < 20; n++){
		
		if(n == 0)
	        System.out.println(0);
	    else if(n == 1)
	      System.out.println(1);
	   else
	      System.out.println((n - 1) + (n - 2));
		}
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}
}