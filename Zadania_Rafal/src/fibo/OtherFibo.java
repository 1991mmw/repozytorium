package fibo;

public class OtherFibo {
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		int initial = 0;
		int next = 1;
		int New = 0;
		System.out.println(initial);
		System.out.println(next);
		for(int i = 0; i < 20; i++) {
			New = initial + next;
			initial = next;
			next = New;
			System.out.println(New);
		}
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}
	
}
