package fibo;

public class FiboRace {

	public static void main(String[] args) {

		Recursive();
		Normal();
	}

	private static void Normal() {
		long startTime = System.nanoTime();
		int initial = 0;
		int next = 1;
		int New = 0;
		System.out.println(initial);
		System.out.println(next);
		for (int i = 0; i < 20; i++) {
			New = initial + next;
			initial = next;
			next = New;
			System.out.println(New);
		}
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}

	private static void Recursive() {
		long startTime = System.nanoTime();
		for (int n = 0; n < 20; n++) {

			if (n == 0)
				System.out.println(0);
			else if (n == 1)
				System.out.println(1);
			else
				System.out.println((n - 1) + (n - 2));
		}
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}
}
