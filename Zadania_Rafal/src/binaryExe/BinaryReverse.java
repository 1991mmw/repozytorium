package binaryExe;

import java.util.Scanner;

public class BinaryReverse {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number: ");
		String binaryNumber = sc.nextLine();
		sc.close();
		int number = Integer.parseInt(binaryNumber,2);
		
		System.out.println(number);
		}
	}

