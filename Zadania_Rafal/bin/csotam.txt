
1. Trójkąt Pascala.
Napisz program rysujący trójkąt Pascala, o zdanej przez użytkownika długości. 
Dokładne wytłumaczenie  sposobu tworzenia wartości w trójkącie znajduje się na Wikipedii : https://pl.wikipedia.org/wiki/Tr%C3%B3jk%C4%85t_Pascala
Do wczytywania danych z konsoli możesz użyć klasy Klawiatura.java załączonej w mailu.

2. Zaimplementuj algorytm znajdujący wartość minimalną i maksymalną w tablicy int[].

Dodatkowe:
Rozmiar tablicy określony jest przez użytkownika. Na dostawie wprowadzonej przez użytkownika liczby, wygenerują tablicę int[] z losowymi wartościami z przedziału 0-100. Możesz użyć do tego metody Math.random

3.  Zaimplementuj algorytm wyliczający i wyświetlający pierwszych n liczb Fibonacciego
Zaimplementuj algorytm :
- korzystając z rekurencji
- liniowy sposób wyliczania (rozważ wykorzystanie trzech zmiennych - jedna przechowująca poprzednią wartość, jedna na aktualną wartość i jedna na wyliczenie następnej wartości na podstawie dwóch poprzednich)

Zastanów się jaka jest złożoność czasowa tych algorytmów. 

Dodatkowo:
Zmierz czas wykonania algorytmów w zależności od rozmiaru danych. Czasy możesz wyeksportować do pliku csv, a następnie otworzyć do w excelu i wygenerować wykres przedstawiający zależność pomiędzy rozmiarem danych a czasem wykonania w obydwu przypadkach.

4. Zaimplementuj algorytm algorytm sumowania kolejnych liczba naturalnych od 1 do n, gdzie n to wartość wprowadzona przez użytkownika, tzn.
Jeśli użytkownik wprowadzi wartość:
- 3 to suma kolejnych liczba naturalnych: 1 + 2 + 3 = 6
- 5 to suma kolejnych liczba naturalnych: 1 + 2 + 3 + 4 + 5 = 15

Zaimplementuj 3 możliwe sposoby uzyskania sumy:
- suma iteracyjna (jak wyżej w przykładach), 
- rekurencyjnie
- skorzystaj ze wzoru na sumę ciągu arytmetycznego.

Metody zaimplementuj w klasie Suma.java . Asercje aktywujemy poprzez dodanie z poziomy command line'a przełącznika "-ea". 
Tutaj jest dokładny opis http://docs.oracle.com/javase/8/docs/technotes/guides/language/assert.html


Rozwiązania możecie umieszczać w BitBucketcie, np. na branchu java_intermediate/lab1 . 
