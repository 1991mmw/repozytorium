package operacjeLiczbowe;

public class Srednia {
	
	static int suma(int[] t) {
		int wynik = 0;
		for(int i = 0; i<t.length; i++) {
			wynik += t[i];
		}
		return wynik;
		
	}
	
	static int srednia(int [] mean) {
		int wynikmean = 0;
		int wynik = 0;
		for(int i = 0; i<mean.length; i++) {
			wynikmean += mean[i];
			int wyniklength = mean.length;
			wynik = wynikmean / wyniklength;
		}
		return wynik;
	}
	
	static int max(int [] max) {
		int wynikmax = max[0];
		for(int x : max) {
			if(x > wynikmax) {
				wynikmax = x;
			}
		}
		
		return wynikmax;
	}
	static int min(int [] min) {
		int wynikmin = min[0];
		for(int x : min) {
			if(x < wynikmin) {
				wynikmin = x;
			}
		}
		
		return wynikmin;
	}
	
	public static void main(String[] args) {
		int[] tab = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,33,44,11,33,88,77,55,44,66} ;
		int wynik, wynikmean, wynikmax, wynikmin, wynikmaxmin;
		
		wynik = suma(tab);
		System.out.println(wynik);
		wynikmean = srednia(tab);
		System.out.println(wynikmean);
		wynikmax = max(tab);
		System.out.println(wynikmax);
		wynikmin = min(tab);
		System.out.println(wynikmin);
		wynikmaxmin = wynikmax - wynikmin;
		System.out.println(wynikmaxmin);
		
		
		
	}

}
