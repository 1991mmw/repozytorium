package bmi;

import java.util.Scanner;

public class BmiB {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double wag = 0, wys = 0, wysokoscwMetrach = 0, wynik = 0;
		System.out.println("Welcome to the BMI calculator, please enter your weight in kilograms");

		while (scan.hasNextLine()) {
			if (scan.hasNextDouble()) {
				wag = scan.nextDouble();
			} else {
				String str = scan.nextLine();
				while (str != null) {
					System.out.println(str);
					if (str.isEmpty()) {
						System.out.println("Read Enter Key.");
					}
					if (scan.hasNextLine()) {
						str = scan.nextLine();
					} else {
						str = null;
					}
				}
			}
			System.out.println("Please enter your height in centimetres.");
			if (scan.hasNextDouble()) {
				wys = scan.nextDouble();
			} else {
				String str = scan.nextLine();
				while (str != null) {
					System.out.println(str);
					if (str.isEmpty()) {
						System.out.println("Read Enter Key.");
					}
					if (scan.hasNextLine()) {
						str = scan.nextLine();
					} else {
						str = null;
					}
				}
			}
			wysokoscwMetrach = wys / 100;
			wynik = wag / (wysokoscwMetrach * wysokoscwMetrach);
			System.out.println("Twoje BMI wynosi: " + wynik);
		}
	}
}