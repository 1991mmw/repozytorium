package bmi;

import javax.swing.JOptionPane;

public class BmiC {

	public static void main(String[] args) {
		String ileWazysz;
		Double waga;
		
		String ileWysokosc;
		Double wysokosc;
		Double wysokoscWMetrach;
		Double wynik;
		
		JOptionPane.showMessageDialog(null, "Kalkulator BMI");
		ileWazysz = JOptionPane.showInputDialog("Ile kilogramow wazysz?");
		waga = Double.parseDouble(ileWazysz);
		
		ileWysokosc = JOptionPane.showInputDialog("Jaki masz wzrost w centymetrach?");
		wysokosc  = Double.parseDouble(ileWysokosc);
		wysokoscWMetrach = wysokosc/100;
		
		wynik = waga / (wysokoscWMetrach*wysokoscWMetrach);
		
		JOptionPane.showMessageDialog(null, "Twoje BMI wynosi: " + wynik);
		
				
	}

}
