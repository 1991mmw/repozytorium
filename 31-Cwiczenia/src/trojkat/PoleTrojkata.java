package trojkat;

import java.util.Scanner;

public class PoleTrojkata {

	public static void main(String[] args) {
		Scanner pyth = new Scanner(System.in);
		double a = 0.0;
		double b = 0.0;
		double c = 0.0;
		String perimeter;
		String area;
		System.out.println("Enter 3 numbers to see if they make a triangle, when finished enter: done");
		
		while(pyth.hasNext()) {
			if(pyth.hasNextDouble()) {
				a = pyth.nextDouble();
				b = pyth.nextDouble();
				c = pyth.nextDouble();
			}
			else { 
				String str = pyth.next();
				if(str.equals("done")) break;
				else {
					System.out.println("Data format error.");
					return;
					
				}
			}
			
		}
		double p = (a+b+c)/2;
		double ar = Math.sqrt(p*(p-a)*(p-b)*(p-c));
		area = Double.toString(ar);
		pyth.close();
		if(ar==0.0) {
			System.out.println("The 3 numbers cannot make a triangle");
		} else {
			System.out.println("Area of triangle is: " + area);
		}
		
		
		
		
		
    }
}
