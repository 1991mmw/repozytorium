package bank;

public class Account {
	
	private String username;
	private String password;
	private int accountNumber;
	private double balance;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public int getAccountNumber() {
		return accountNumber;
	}
	public double getBalance() {
		return balance;
	}
	public double withdraw(double amount) {
		balance -= amount;
		return balance;
	}
	public double deposit(double amount) {
		balance += amount;
		return balance;
		
	}
	public Account(String username, String password, int accountNumber, double balance) {
		this.username = username;
		this.password = password;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Account [username=" + username + ", password=" + password + ", accountNumber=" + accountNumber
				+ ", balance=" + balance + "]";
	}
	
	
	

}
