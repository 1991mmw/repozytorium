package bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		HashMap<Integer, Account> accountMap = new HashMap<Integer, Account>();

		System.out.println("*****************************************");
		System.out.println("*           WELCOME TO BANK             *");
		System.out.println("*                  *                    *");
		System.out.println("*     REGISTER     OR     SIGN IN       *");
		System.out.println("*****************************************");

		initial();
	}

	private static void initial() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		String signUp = scanner.nextLine();
		if (signUp.equalsIgnoreCase("REGISTER")) {
			register();
		} else if (signUp.equalsIgnoreCase("SIGN IN")) {
			signIn();
		} else {
			System.out.println("Enter either Register or Sign in.");
			initial();
		}

	}

	private static void register() {
		HashMap<Integer, Account> accountMap = new HashMap<Integer, Account>();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a username: ");
		String username = scanner.nextLine();
		System.out.println("Enter a password: ");
		String password = scanner.nextLine();
		int accountNumber = (int) (Math.random() * 999999);
		System.out.println("This is your Account Number. " + accountNumber + " Write it down!");
		System.out.println("Enter initial amount: ");
		double balance = scanner.nextDouble();

		List<Account> Accounts = new ArrayList<Account>();

		Account newAccount = new Account(username, password, accountNumber, balance);
		newAccount.setPassword(password);
		newAccount.setUsername(username);
		newAccount.setBalance(balance);
		newAccount.setAccountNumber(accountNumber);
		Accounts.add(new Account(username, password, accountNumber, balance));
		accountMap.put(accountNumber, newAccount);
		System.out.println(accountMap);

		PrintWriter printwriter = null;
		try {
			printwriter = new PrintWriter(new FileOutputStream("Account.txt", true));
			StringBuilder stringbuilder = new StringBuilder();
			for (Account a : Accounts) {
				stringbuilder.append(a.getAccountNumber()).append("-").append(a.getUsername()).append("-")
						.append(a.getPassword()).append("-").append(a.getBalance());

			}
			printwriter.flush();
			printwriter.println(stringbuilder);
			printwriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Thank you for registering with Bank.");
		initial();
	}

	private static void signIn() {
		System.out.println("Enter your username: ");
		Scanner scanner = new Scanner(System.in);
		String checkUsername = scanner.nextLine();
		File file = new File("Account.txt");
		Scanner in = null;
		String line = null;
		try {
			in = new Scanner(file);
			while (in.hasNext()) {
				line = in.nextLine();
				if (line.contains(checkUsername)) {
					// System.out.println(line);
					break;
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			in.close();
		}

		String account, username1, password, amount;
		String[] currentAccount = new String[4];
		currentAccount = line.split("-");
		account = currentAccount[0];
		username1 = currentAccount[1];
		password = currentAccount[2];
		amount = currentAccount[3];
		int accountNumber = Integer.parseInt(account);
		double balance = Double.parseDouble(amount);
		Account savedAccount = new Account(username1, password, accountNumber, balance);
		// System.out.println(savedAccount);
		System.out.println("Enter password: ");
		String enteredPassword = scanner.nextLine();
		if (enteredPassword.equals(password)) {
			homePage(savedAccount);
		} else {
			System.out.println("The username and/or password you entered is Incorrect! Try Again.");
			signIn();

		}

	}

	private static void homePage(Account savedAccount) {
		System.out.println("Welcome " + savedAccount.getUsername() + " to your Personal Home Page");
		System.out.println("*****************************************");
		System.out.println("*     CHOOSE FROM FOLLOWING OPTIONS:    *");
		System.out.println("*****************************************");
		System.out.println("*      BALANCE           TRANSFER       *");
		System.out.println("*                                       *");
		System.out.println("*      WITHDRAW          DEPOSIT        *");
		System.out.println("*                                       *");
		System.out.println("*    CHANGE PASSWORD      EXIT          *");
		System.out.println("*****************************************");

		secondary(savedAccount);

	}

	private static void secondary(Account savedAccount) {
		Scanner scanner = new Scanner(System.in);
		String option = scanner.nextLine();
		if (option.equalsIgnoreCase("BALANCE")) {
			System.out.println("Your current balance: $" + savedAccount.getBalance());
			secondary(savedAccount);
		} else if (option.equalsIgnoreCase("TRANSFER")) {
			transfer(savedAccount);
		} else if (option.equalsIgnoreCase("WITHDRAW")) {
			withdraw(savedAccount);
		} else if (option.equalsIgnoreCase("DEPOSIT")) {
			deposit(savedAccount);
		} else if (option.equalsIgnoreCase("CHANGE PASSWORD")) {
			changepassword(savedAccount);
		} else if (option.equalsIgnoreCase("EXIT")) {
			exit(savedAccount);
		}
	}

	private static void exit(Account savedAccount) {
		File inputFile = new File("Account.txt");
//		System.out.println(inputFile.getAbsolutePath());
		File tempFile = new File("TempFile.txt");
//		System.out.println(tempFile.getAbsolutePath());
		try {
			PrintWriter writer = new PrintWriter(new FileOutputStream(tempFile, true));

			try {
				Scanner in = new Scanner(new FileInputStream(inputFile));
				String line = null;
				while (in.hasNext()) {
					line = in.nextLine();
					if (!line.contains(savedAccount.getUsername())) {
//						System.out.println(line);
						writer.println(line);
					} else if (!in.hasNext()) {
						break;
					} else {
//						System.out.println("My line " + line);
					}
				}
				writer.close();
				in.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(new FileOutputStream(tempFile, true));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder.append(savedAccount.getAccountNumber()).append("-").append(savedAccount.getUsername()).append("-")
				.append(savedAccount.getPassword()).append("-").append(savedAccount.getBalance());

		printWriter.println(stringbuilder);
		printWriter.close();
		System.out.println(savedAccount);
		inputFile.setWritable(true);
		tempFile.setWritable(true);
		inputFile.delete();
		tempFile.renameTo(inputFile);
		System.out.println("Thank you for using our Bank. Have a nice day!");
	
	}

	private static void changepassword(Account savedAccount) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter old password: ");
		String oldPassword = sc.nextLine();
		if(oldPassword.contains(savedAccount.getPassword())){
			System.out.println("Enter new password: ");
			String newPassword = sc.nextLine();
			savedAccount.setPassword(newPassword);
			System.out.println("Your password has been changed.");
			homePage(savedAccount);
		} else {
			changepassword(savedAccount);
		}
	}

	private static void deposit(Account savedAccount) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter amount you would like to deposit: ");
		double enterDeposit = scanner.nextDouble();
		double newDeposit = savedAccount.deposit(enterDeposit);
		System.out.println(newDeposit);
		secondary(savedAccount);

	}

	private static void withdraw(Account savedAccount) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter amount you would like to withdraw: ");
		double enterWithdraw = scanner.nextDouble();
		double newWithdraw = savedAccount.withdraw(enterWithdraw);
		System.out.println(newWithdraw);
		secondary(savedAccount);
	}

	private static void transfer(Account savedAccount) {
		
	}
}