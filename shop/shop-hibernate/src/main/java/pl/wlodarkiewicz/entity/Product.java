package pl.wlodarkiewicz.entity;

import java.math.BigDecimal;

@Entity
@Table(name = "PRODUCT")
public class Product {
	
	private Integer id;
	private String name;
	private Integer quantity;
	private BigDecimal price;
}
