package game;
import javax.swing.JOptionPane;

import game.Tictactoe;

public class Main {
	public static boolean playerTurn = true;
	public static boolean playerWon = false;
	public static boolean computerWon = false;
	public static boolean draw = false;
	
	public static Tictactoe board = new Tictactoe();
	
	public static void main(String[] args) {
		if(board.isVisible() == false ) {
			board.setVisible(true);
		}
	}
	
	public static void checkForWin(){
		if(board.jButton1.getText().equals("X")){
			if(board.jButton2.getText().equals("X")){
				if(board.jButton3.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton1.getText().equals("X")){
			if(board.jButton5.getText().equals("X")){
				if(board.jButton9.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton1.getText().equals("X")){
			if(board.jButton4.getText().equals("X")){
				if(board.jButton7.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton2.getText().equals("X")){
			if(board.jButton5.getText().equals("X")){
				if(board.jButton8.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton3.getText().equals("X")){
			if(board.jButton6.getText().equals("X")){
				if(board.jButton9.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton4.getText().equals("X")){
			if(board.jButton5.getText().equals("X")){
				if(board.jButton6.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton7.getText().equals("X")){
			if(board.jButton8.getText().equals("X")){
				if(board.jButton9.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton3.getText().equals("X")){
			if(board.jButton5.getText().equals("X")){
				if(board.jButton7.getText().equals("X")){
					playerWon = true;
					computerWon = false;
					JOptionPane.showMessageDialog(null, "Player 'X' has won!");
				}
			}
		}
		if(board.jButton1.getText().equals("O")){
			if(board.jButton2.getText().equals("O")){
				if(board.jButton3.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton1.getText().equals("O")){
			if(board.jButton5.getText().equals("O")){
				if(board.jButton9.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton1.getText().equals("O")){
			if(board.jButton4.getText().equals("O")){
				if(board.jButton7.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton2.getText().equals("O")){
			if(board.jButton5.getText().equals("O")){
				if(board.jButton8.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton3.getText().equals("O")){
			if(board.jButton6.getText().equals("O")){
				if(board.jButton9.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton4.getText().equals("O")){
			if(board.jButton5.getText().equals("O")){
				if(board.jButton6.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton7.getText().equals("O")){
			if(board.jButton8.getText().equals("O")){
				if(board.jButton9.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(board.jButton3.getText().equals("O")){
			if(board.jButton5.getText().equals("O")){
				if(board.jButton7.getText().equals("O")){
					playerWon = false;
					computerWon = true;
					JOptionPane.showMessageDialog(null, "Player 'O' has won!");
				}
			}
		}
		if(!board.jButton1.getText().isEmpty()){
			if(!board.jButton2.getText().isEmpty()){
				if(!board.jButton3.getText().isEmpty()){
					if(!board.jButton4.getText().isEmpty()){
						if(!board.jButton5.getText().isEmpty()){
							if(!board.jButton6.getText().isEmpty()){
								if(!board.jButton7.getText().isEmpty()){
									if(!board.jButton8.getText().isEmpty()){
										if(!board.jButton9.getText().isEmpty()){
											if(playerWon == false && computerWon == false){
											JOptionPane.showMessageDialog(null, "Game has ended in a draw!");
											draw = true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if(playerWon == true || computerWon == true || draw == true) {
			board.setVisible(false);
		}
		
	}

}
