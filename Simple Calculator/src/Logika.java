

public class Logika {
    
    public static double policz(double x, double y, String operacja) {
        switch(operacja) {
            case "+" : return x + y;
            case "-" : return x - y;
            case "*" : return x * y;
            case "/" : return x / y;
            default  : return 0; // w razie gdyby ktos wywolal metode z inna operacja
        }
    }
} 
