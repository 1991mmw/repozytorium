package clientTest;

import java.util.Date;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class ClientExample {
	public static void main(String[] args) {

		MongoClient mongo = new MongoClient("localhost", 8080);

		MongoDatabase db = mongo.getDatabase("michal");
		System.out.println(db.getName());

		for (String dba : db.listCollectionNames()) {
			System.out.println(dba);
		}

		MongoCollection<Document> table = db.getCollection("user");

		/**** Insert ****/
		// create a document to store key and value
		Document document = new Document();
		document.put("name", "mike");
		document.put("age", 25);
		document.put("createdDate", new Date());
		table.insertOne(document);

		/**** Find and display ****/
		Document searchQuery = new Document();
		searchQuery.put("name", "mike");

		FindIterable<Document> cursor = table.find(searchQuery);

		MongoCursor<Document> it = cursor.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}

		// while (cursor.hasNext()) {
		// System.out.println(cursor.next());
		// }
		for (String dba : mongo.listDatabaseNames()) {
			System.out.println(dba);
		}
	}
}
