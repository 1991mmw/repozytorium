package swingTest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class SimpleSwingExample extends JFrame {
	private JPanel createMainPanel() {
		JLabel label = new JLabel("First Swing label");
		JPanel panel = new JPanel();
		panel.add(label);

		MouseListener l = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Pressed Mouse");
			}
		};
		label.addMouseListener(l);
		
		JButton button = new JButton("OK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Clicked OK");
			}
		});
		
		panel.add(button);
		
		ButtonGroup bg = new ButtonGroup();
		
		JRadioButton jRadioButton1 = new JRadioButton("Button 1");
		JRadioButton jRadioButton2 = new JRadioButton("Button 2");
		
		bg.add(jRadioButton1);
		bg.add(jRadioButton2);
		
		jRadioButton1.setSelected(true);
		
		panel.add(jRadioButton1);
		panel.add(jRadioButton2);
		
		return panel;
	}

	public static void main(String[] argv) {
		SimpleSwingExample exampleFrame = new SimpleSwingExample();
		exampleFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		exampleFrame.add(exampleFrame.createMainPanel());
		exampleFrame.pack();
		exampleFrame.setVisible(true);
	}
}
